import "./App.scss";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import FrontPage from "../pages/FrontPage";
import TVShows from "../pages/TVShows";
// import TVShow from "../pages/TVShow";
import Movies from "../pages/Movies";
import Movie from "../pages/Movie";
import React from "react";

function App() {
  return (
    <BrowserRouter>
      <React.Fragment>
        <Switch>
          <Route path="/" component={FrontPage} exact />
          <Route path="/Movies" component={Movies} exact />
          <Route path="/Tv" component={TVShows} exact />
          <Route path="/:movieId" component={Movie} exact />
        </Switch>
      </React.Fragment>
    </BrowserRouter>
  );
}

export default App;
