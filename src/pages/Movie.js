import React, { Component } from "react";
import MovieInfo from "../components/MovieInfo/MovieInfo";

import {
  apiUrl,
  apiVerOne,
  apiKey,
  imgBaseUrl,
  backdropSize,
  posterSize,
} from "../api/settings.js";

export class Movie extends Component {
  constructor(props) {
    super(props);
    this.state = {
      movie: null,
      actors: null,
      directors: [],
    };
  }

  componentDidMount() {
    const endpoint = `${apiUrl}/${apiVerOne}/movie/${this.props.match.params.movieId}?api_key=${apiKey}&language=en-US&page=1`;
    this.fetchItems(endpoint);
  }

  fetchItems = (endpoint) => {
    fetch(endpoint)
      .then((result) => result.json())
      .then((result) => {
        this.setState({ movie: result }, () => {
          // fetch actors
          const endpoint = `${apiUrl}/${apiVerOne}/movie/${this.props.match.params.movieId}/credits?api_key=${apiKey}&language=en-US&page=1`;
          fetch(endpoint)
            .then((result) => result.json())
            .then((result) => {
              const directors = result.crew.filter(
                (member) => member.job === "Director"
              );
              this.setState({
                actors: result.cast,
                directors: directors,
              });
            });
        });
      });
  };

  render() {
    return (
      <div>
        <MovieInfo movie={this.state.movie} />
      </div>
    );
  }
}

export default Movie;
