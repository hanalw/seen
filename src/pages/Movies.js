import React, { Component } from "react";
import {
  apiUrl,
  apiVerOne,
  apiKey,
  imgBaseUrl,
  backdropSize,
  posterSize,
} from "../api/settings";
import ListItem from "../components/ListItem/ListItem";
import HeroImage from "../components/HeroImage/HeroImage";

import "../styles/Elements.scss";

export class Movies extends Component {
  constructor(props) {
    super(props);
    this.state = {
      movies: [],
      loading: false,
      heroImage: null,
    };
  }

  componentDidMount() {
    this.setState({ loading: true });
    this.fetchMovies();
  }

  fetchMovies = () => {
    const endpoint = `${apiUrl}/${apiVerOne}/movie/popular?api_key=${apiKey}&language=en-US&page=1`;
    fetch(endpoint)
      .then((result) => result.json())
      .then((result) => {
        this.setState({
          movies: [...this.state.movies, ...result.results],
          loading: false,
          heroImage: result.results[0].backdrop_path,
        });
      });
  };

  fetchNewMovies = (e) => {
    const value = e.target.value;
    const endpoint = `${apiUrl}/${apiVerOne}/movie/${value}?api_key=${apiKey}&language=en-US&page=1`;
    fetch(endpoint)
      .then((result) => result.json())
      .then((result) => {
        this.setState({
          movies: result.results,
          loading: false,
        });
      });
  };

  render() {
    return (
      <div>
        {this.state.heroImage ? (
          <HeroImage
            image={`${imgBaseUrl}/${backdropSize}/${this.state.heroImage}`}
          />
        ) : (
          ""
        )}
        <h2>Movies</h2>
        <div>
          <button onClick={this.fetchNewMovies} value="popular">
            Popular
          </button>
          <button onClick={this.fetchNewMovies} value="top_rated">
            Top rated
          </button>
        </div>
        <div>
          {this.state.movies
            .filter(
              (movie) =>
                movie.original_language === "en" ||
                movie.original_language === "ko"
            )
            .map((movie) => (
              <ListItem
                clickable="true"
                key={movie.id}
                movie={movie}
                image={`${imgBaseUrl}/${posterSize}/${movie.poster_path}`}
              />
            ))}
        </div>
      </div>
    );
  }
}

export default Movies;
