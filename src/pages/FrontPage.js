import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./FrontPage.scss";

export class FrontPage extends Component {
  render() {
    return (
      <React.Fragment>
        <div className="Item">
          <Link to={{ pathname: "Movies" }} />
          <h3>Movies</h3>
        </div>
        <div className="Item">
          <Link to={{ pathname: "Tv" }} />
          <h3>TV Shows</h3>
        </div>
      </React.Fragment>
    );
  }
}

export default FrontPage;
