import React, { Component } from "react";
import {
  apiUrl,
  apiVerOne,
  apiKey,
  imgBaseUrl,
  backdropSize,
  posterSize,
} from "../api/settings";
import HeroImage from "../components/HeroImage/HeroImage";

import ListItem from "../components/ListItem/ListItem";
export class TVShows extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tvShows: [],
      loading: false,
      heroImage: null,
    };
  }

  componentDidMount() {
    this.setState({ loading: true });
    const endpoint = `${apiUrl}/${apiVerOne}/tv/popular?api_key=${apiKey}&language=en-US&page=1`;
    this.fetchTv(endpoint);
  }

  fetchTv = (endpoint) => {
    fetch(endpoint)
      .then((result) => result.json())
      .then((result) => {
        this.setState({
          tvShows: [...this.state.tvShows, ...result.results],
          loading: false,
          heroImage: result.results[0].backdrop_path,
        });
      });
  };

  fetchNewTv = (e) => {
    const value = e.target.value;
    console.log(value);
    const endpoint = `${apiUrl}/${apiVerOne}/tv/${value}?api_key=${apiKey}&language=en-US&page=1`;
    fetch(endpoint)
      .then((result) => result.json())
      .then((result) => {
        this.setState({
          tvShows: result.results,
          loading: false,
        });
      });
  };

  render() {
    return (
      <React.Fragment>
        {this.state.heroImage ? (
          <HeroImage
            image={`${imgBaseUrl}/${backdropSize}/${this.state.heroImage}`}
          />
        ) : (
          ""
        )}
        <h2>Tv</h2>
        <div>
          <button onClick={this.fetchNewTv} value="popular">
            Popular
          </button>
          <button onClick={this.fetchNewTv} value="top_rated">
            Top rated
          </button>
        </div>
        {this.state.tvShows
          .filter(
            (movie) =>
              movie.original_language === "en" ||
              movie.original_language === "ko"
          )
          .map((tv) => (
            <ListItem
              key={tv.id}
              tv={tv}
              image={`${imgBaseUrl}/${posterSize}/${tv.poster_path}`}
            />
          ))}
      </React.Fragment>
    );
  }
}

export default TVShows;
