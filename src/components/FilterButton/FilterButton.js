import React, { Component } from "react";
import { apiUrl, apiVerOne, apiKey } from "../../api/settings";

export class FilterButton extends Component {
  constructor(props) {
    super(props);
    this.state = {
      btnValue: this.props.value,
    };
  }

  sendData = () => {
    const value = this.state.btnValue;
    this.props.parentCallback(value);
  };
  render() {
    return (
      <div>
        <button onClick={this.sendData}>{this.props.title}</button>
      </div>
    );
  }
}

export default FilterButton;
