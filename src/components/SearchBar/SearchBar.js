import React, { Component } from "react";
import "./SearchBar.scss";

export class SearchBar extends Component {
  render() {
    return (
      <div className="search">
        <h4>SearchBar</h4>
        <input type="text" />
      </div>
    );
  }
}

export default SearchBar;
