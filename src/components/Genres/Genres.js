import React, { Component } from 'react'
import {apiUrl, apiVerOne, apiKey} from '../../api/settings';
export const MContext = React.createContext();

export class Genres extends Component {
  constructor(props) {
    super(props);
    this.state = {
      genres: []
    }
  }

  componentDidMount() {
    const endpoint = `${apiUrl}/${apiVerOne}/genre/movie/list?api_key=${apiKey}&language=en-US&page=1`;
    console.log(endpoint)
    fetch(endpoint)
    .then(result => result.json())
    .then(result => {
      this.setState({genres: result.genres})
    })
  }

  sendToParent = () => {
    this.props.parentCallBack(this.state.genres)
    console.log(this.state.genres)
  }

  render() {
    return (
      <div>
        <h2>Genre</h2>
      </div>
    )
  }
}

export default Genres
