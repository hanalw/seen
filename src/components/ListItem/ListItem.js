import React from "react";
import { Link } from "react-router-dom";
import "../../styles/Elements.scss";
import "./ListItem.scss";

const ListItem = (props) => {
  return (
    <div className="movie-item">
      <div className="img-container movie-item-img">
        {props.clickable ? (
          <Link
            to={{
              pathname: `/${props.movieId}`,
              movieName: `${props.movieName}`,
            }}
          >
            <div
              className="img"
              style={{ backgroundImage: `url('${props.image}')` }}
            ></div>
          </Link>
        ) : (
          <div
            className="img"
            style={{ backgroundImage: `url('${props.image}')` }}
          ></div>
        )}
      </div>
    </div>
  );
};

export default ListItem;
