import React from 'react';
import './HeroImage.scss';

const HeroImage = props => {
  return (
    <div className="heroimg" style={{backgroundImage:`url('${props.image}')`}}>
    </div>
  )
}

export default HeroImage