const apiUrl = "https://api.themoviedb.org";
const apiVerOne = "3";
const apiVerTwo = "4";
const apiKey = "4877076bffe0cd6c8ec92a6a789199f0";
const apiToken = "eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI0ODc3MDc2YmZmZTBjZDZjOGVjOTJhNmE3ODkxOTlmMCIsInN1YiI6IjVjZWI5YTdkOTI1MTQxNjY5ZWJiZWY4YiIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.WqrwqujhICaedioPI9POwLKJMMMsE9onId5Cv5_AR3Q";
const imgBaseUrl = "http://image.tmdb.org/t/p";
const backdropSize = "w1280";
const posterSize = "w500";

export {
    apiUrl,
    apiVerOne,
    apiVerTwo,
    apiKey,
    apiToken,
    imgBaseUrl,
    backdropSize,
    posterSize
}